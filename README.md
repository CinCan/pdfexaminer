# pdfexaminer  

Python API for [https://www.pdfexaminer.com/](https://www.pdfexaminer.com/)  

## Usage  

```bash
$ python pdfexaminer.py --help


usage: pdfexaminer.py [-h] -i INPUT [-f FORMAT]

PDFExaminer

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
  -f FORMAT, --format FORMAT

format options:
  json, xml, ioc, php, severity, rating, is_malware, text, summary
```

### LICENSE  

[MIT](https://opensource.org/licenses/MIT)  
