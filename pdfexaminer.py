import os.path
import argparse
import time
import threading
import sys
import requests


class PdfexaminerSendfile:
    def __init__(self):
        self.__report = ""

    def get_report(self):
        self.__report = self.__report.text.strip()
        return self.__report

    def set_report(self, report):
        self.__report = report


    # Progress bar
    @staticmethod
    def progress_bar():
        thread_two = threading.currentThread()
        progress_x = 0
        loading = ["_", "-", "¯", "-"]
        while getattr(thread_two, "run", True):
            time.sleep(0.2)
            if progress_x < len(loading)-1:
                progress_x = progress_x + 1
            else:
                progress_x = 0
            print("  Loading " + loading[progress_x], end="\r")


    # Parse passed arguments
    @staticmethod
    def parse_arguments(formats):
        parser = argparse.ArgumentParser(description='PDFExaminer')
        parser.add_argument('-i', '--input', required=True, help="")
        parser.add_argument('-f', '--format', required=False, help=formats)
        return parser.parse_args()


    # Send file
    def send_file(self, input_file, datatype, formats):
        url = 'https://www.pdfexaminer.com/pdfapi.php'
        files = {'sample[]': open(input_file, 'rb')}
        try:
            request = requests.get(url, files=files, data=datatype)
        except requests.exceptions.RequestException as e:
            print("Connection failed")
            raise SystemExit(e)

        self.set_report(request)


# Main
def main():
    pdfx = PdfexaminerSendfile()
    formats = "json, xml, ioc, php, severity, rating, is_malware, text, summary"
    args = pdfx.parse_arguments(formats)

    # Check that input file exists
    if not os.path.exists(args.input):
        print("File not found")
        sys.exit()

    # Set format, JSON as default
    if not args.format:
        datatype = {'type':'json'}
    elif args.format in formats:
        datatype = {'type':args.format}
    else:
        print("Unidentified format")
        sys.exit()

    thread_one = threading.Thread(target=pdfx.send_file, args=(args.input, datatype, formats,))
    thread_two = threading.Thread(target=pdfx.progress_bar)
    thread_one.start()
    thread_two.start()
    thread_one.join()

    # Stop progress bar when send_file finishes
    if not thread_one.isAlive():
        thread_two.run = False

    # Print out report
    try:
        print(pdfx.get_report())
    except:
        pass


if __name__ == '__main__':
    main()
